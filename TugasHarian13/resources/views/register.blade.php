<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up Page</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name:</label> <br> <br>
        <input type="text" name="firstName"> <br> <br>

        <label>Last Name:</label> <br> <br>
        <input type="text" name="lastName"> <br> <br>

        <label>Email:</label> <br> <br>
        <input type="email"> <br> <br>

        <label>Password:</label> <br> <br>
        <input type="password"> <br> <br>

        <label>Gender:</label> <br> <br>
        <input type="radio" name="Gender">Male <br>
        <input type="radio" name="Gender">Female <br>
        <input type="radio" name="Gender">Other <br> <br>

        <label>Nationality:</label> <br> <br>
        <select name="Nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select> <br> <br>

        <label>Language Spoken:</label> <br> <br>
        <input type="checkbox" name="language"> Bahasa Indonesia <br>
        <input type="checkbox" name="language"> English <br>
        <input type="checkbox" name="language"> Other <br> <br>

        <label>Bio:</label> <br> <br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br>

        <label>Date of birth: </label> <br> <br>
        <input type="date"> <br> <br>

        <input type="submit" value="Sign up">
    </form>

    
</body>
</html>