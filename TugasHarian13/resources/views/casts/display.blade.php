@extends('layout.master')

@section('title')
Halaman Tampil Cast
@endsection

@section('sub-title')
Cast
@endsection

@section('content')

<a href="/cast/create/" class="btn btn-primary btn-sm my-3">Tambah</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Age</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($casts as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->age}}</td>
                <td>
                    <form action="/cast/{{$item->id}}" method="POST">
                        @method('delete')
                        @csrf
                        <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">

                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>
                    Data Kosong Belum Terisi
                </td>
            </tr>
        @endforelse
    </tbody>
  </table>
@endsection