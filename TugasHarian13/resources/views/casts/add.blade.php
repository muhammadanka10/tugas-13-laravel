@extends('layout.master')

@section('title')
Halaman Tambah Cast
@endsection

@section('sub-title')
Cast
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label>Cast Name</label>
      <input type="text" class="form-control" name="name">
    </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Cast Age</label>
      <input type="number" class="form-control" name="age">
    </div>
    @error('age')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Cast Bio</label>
        <textarea name="bio" class="form-control"></textarea>
      </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection