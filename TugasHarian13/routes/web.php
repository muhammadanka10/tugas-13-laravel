<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);

Route::get('/register', [AuthController::class, 'regist']);

Route::post('/welcome', [AuthController::class, 'greeting']);

Route::get('/table', function(){
    return view('table');
});

Route::get('/data-table', function(){
    return view('datatable');
});

// CRUD Cast
//Create Data
//Route untuk mengarah ke form tambah kategori
Route::get('/cast/create', [CastController::class, 'create']);
//Route untuk menyimpan data inputan ke table kategori DB
Route::post('/cast', [CastController::class, 'store']);

//Read Data
//Route untuk menampilkan semua data di table Casts DB
Route::get('/cast', [CastController::class, 'index']);
//Route untuk menampilkan detail data berdasarkan id
Route::get('/cast/{id}', [CastController::class, 'show']);

//Update Data
//Route untuk mengarah ke form edit cast
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
//Route untuk update data berdasarkan di table cast DB
Route::put('/cast/{id}', [CastController::class, 'update']);

//Delete Data
//Route untuk delete data cast berdasarkan id
Route::delete('/cast/{id}', [CastController::class, 'destroy']);