<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regist(){
        return view('register');
    }

    public function greeting(Request $request){
        $namaDepan = $request['firstName'];
        $namaBelakang = $request['lastName'];
 
        return view('welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
     }
}
