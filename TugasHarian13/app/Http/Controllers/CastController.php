<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create(){
        return view('casts.add');
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required',
            'age' => 'required',
            'bio' => 'required',
        ],
        [
            'name.required' => 'name harus diisi!',
            'age.required' => 'age harus diisi!',
            'bio.required' => 'bio harus diisi!',
        ]);

        DB::table('casts')->insert([
            'name' => $request['name'],
            'age' => $request['age'],
            'bio' => $request['bio'],
        ]);

        return redirect('/cast');
    }

    public function index(){
        $casts = DB::table('casts')->get();
 
        return view('casts.display', ['casts' => $casts]);
    }

    public function show($id){
        $cast = DB::table('casts')->find($id);

        return view('casts.detail', ['cast' => $cast]);
    }

    public function edit($id){
        $cast = DB::table('casts')->find($id);

        return view('casts.edit', ['cast' => $cast]);
    }

    public function update($id, Request $request){
        $request->validate([
            'name' => 'required',
            'age' => 'required',
            'bio' => 'required',
        ],
        [
            'name.required' => 'name harus diisi!',
            'age.required' => 'age harus diisi!',
            'bio.required' => 'bio harus diisi!',
        ]);

        DB::table('casts')
              ->where('id', $id)
              ->update(
                [
                    'name' => $request['name'],
                    'age' => $request['age'],
                    'bio' => $request['bio']
                ]
            );
        return redirect('/cast');
    }

    public function destroy($id){
        DB::table('casts')->where('id', $id)->delete();

        return redirect('/cast');
    }
}
